console.log("Hello B248!");

/*
	JSON
		JSON stands for JavaScript Object Notation
		JSON is also used in other programming langauges

		Syntax:

		{
			"propertyA" : "valueA",
			"propertyB" : "valueB"
		};
			
		JSON are wrapped in curly braces
		Properties/keys are wrapped in double quotations
*/

		let sample1 = `

			{
				"name": "Cardo Dalisay",
				"age": 20,
				"address": {
					"city": "Quezon City",
					"country": "Philippines"
				}				
			}
		`;


		console.log(sample1);

		// Create a variable that will hold a "JSON" and create a person object (2 min); 6:00PM
			//log and send ss
		
		let sample2 = `

			{
				"name": "Cee Dee",
				"age": 18,
				"address": {
					"city": "Quezon City",
					"country": "Philippines"
				}				
			}

		`
		console.log(sample2);
		console.log(typeof sample1);//string

		//Are we able to turn a JSON into a JS Object?
		//JSON.parse() - will return the JSON as an Object

		console.log(JSON.parse(sample1));
		console.log(JSON.parse(sample2));

		//JSON Array
		//JSON array is an array of JSON

		let sampleArr = `

			[
				{
					"email":"sinayangmoko@gmail.com",
					"password":"february14",
					"isAdmin":false
				},
				{
					"email":"dalisay@cardo.com",
					"password":"thecountryman",
					"isAdmin":false
				},
				{
					"email":"jsonv@gmail.com",
					"password":"friday13",
					"isAdmin":true
				}
			]
		`;

		console.log(sampleArr);
		console.log(typeof sampleArr);//string

		//Can we use Array Methods on a JSON Array?
		//No. Because a JSON is a string

		//so what can we do to be able to add more items/objects into our sampleArr?

		//PARSE the JSON array to a JS array and save it in a variable

		let parsedSampleArr = JSON.parse(sampleArr);
		console.log(parsedSampleArr);
		console.log(typeof parsedSampleArr);//object - array
		
		//can we now delete the last item in the JSON Array?

		console.log(parsedSampleArr.pop());
		console.log(parsedSampleArr);

		//if for example we need to send this data back to our client/front end it should be in JSON format

		//JSON.parse() does not mutate or update the original JSON
		//we can actually turn a JS object into a JSON
		//JSON.stringify() - this will stringify JS objects as JSON

		sampleArr = JSON.stringify(parsedSampleArr);

		console.log(sampleArr);

		//Database (JSON) => Server/API (JSON to JS Object to process) => sent as JSON => frontend/client

		/*
			Mini Activity (5min);
			Given the JSON array, process it and convert to a JS Object so we can manipulate the array

			Delete the last item in the array and add a new item in the array

			Stringify the array back in JSON

			and update jsonArr with the stringified array

		*/


		let jsonArr = `

			[
				"pizza",
				"hamburger",
				"spaghetti",
				"shanghai",
				"hotdog stick on a pineapple",
				"pancit bihon"
			]

		`
		console.log(jsonArr);
		let parsedJsonArr = JSON.parse(jsonArr);

		parsedJsonArr.pop();
		parsedJsonArr.push("ice cream!");

		jsonArr = JSON.stringify(parsedJsonArr);
		console.log(jsonArr);


		//Gather User Details

		let firstName = prompt("What is your first name?");
		let lastName = prompt("What is your last name?");
		let age = prompt("What is your age?");
		let address = {
			city: prompt("Which city do you live in?"),
			country: prompt("Which country does your city address belong to?")
		};

		let otherData = JSON.stringify({
		
				firstName: firstName,
				lastName: lastName,
				age: age,
				address: address
			})

		console.log(otherData);


		let sample3 = `

			{
				'name': "Tolits",
				"age": 18,
				"address": {
					"city": "Quezon City",
					"country": "Philippines"
				}				
			}

		`
		console.log(sample2);
		try{
			console.log(JSON.parse(sample3));
		}catch(err){
			console.log(err)
		}finally{
			console.log("Line 181 will result to an error. Use double quotes for properties.")
		}
